#include "ucos_ii.h"
#include "AdvanceTim.h"
#include "myiic.h"
#include "mpu6050.h"
#include "delay.h"
#include "sys.h"
#include "usart.h"
#include "sysTick.h"



//加速度传感器原始数据
short aacx,aacy,aacz;		
//陀螺仪原始数据
short gyrox,gyroy,gyroz;
//GY86数据数组
char tmp[7];

extern u8 TIM1CH1_CAPTURE_STA ;
extern u16 TIM1CH1_CAPTURE_VAL;


//设置优先级
#define TASK_INIT_PRIO    5	//最先执行初始化
#define TASK_MPU6050_PRIO    30
#define TASK_HM10_PRIO    25 
#define TASK_PWM_PRIO    15
//#define TASK_RECEIVER_PRIO    20


//设置任务堆栈大小
#define TASK_INIT_STK_SIZE			128
#define TASK_MPU6050_STK_SIZE			128
#define TASK_HM10_STK_SIZE			128
#define TASK_PWM_STK_SIZE			128
//#define TASK_RECEIVER_STK_SIZE			128

//任务堆栈定义
OS_STK TASK_INIT_STK[TASK_INIT_STK_SIZE];
OS_STK TASK_HM10_STK[TASK_HM10_STK_SIZE];
OS_STK TASK_MPU6050_STK[TASK_MPU6050_STK_SIZE];
OS_STK TASK_PWM_STK[TASK_PWM_STK_SIZE];
//OS_STK TASK_RECEIVER_STK[TASK_RECEIVER_STK_SIZE];

//信号量定义
OS_EVENT  *Synchronize;
OS_EVENT	*ReceiverSem;

//模块初始化
void TASK_INIT(void *pdata){
	
	//初始化延时函数
	delay_init(168);
	//初始化时钟
	//应该为arr:16位设置为最大， psc设置来为1MHz
	TIM1_Cap_Init(0xFFFF, 84-1);			
	
	//初始化USART
	USART_Config1();	//USART2
	//USART6_蓝牙初始化
	USART_Config2();
  USARTSendString(USART6,"HM10 INIT OK");	
	
	//初始化MPU6050
	MPU_Init();	//在MPU6050初始化内部就已经初始化了IIC
	USARTSendString(USART6,"MPU6050 INIT OK");	
	
	USARTSendString(USART6,"RECEIVER INIT OK");	
	//初始化接收机

	//删除任务初始化的优先级
	OSTaskDel(TASK_INIT_PRIO);
}


void TASK_PWM(void *pdata){
	u32 temp = 0;
	float set = 0;
	INT8U err;
	
	while(1){
		OSSemPend(ReceiverSem,100,&err);
		if(TIM1CH1_CAPTURE_STA&0x80)//成功捕获一次上升沿
		{
			temp = TIM1CH1_CAPTURE_STA&0X3F;
			temp *= 65536;										 //溢出时间总和。
			temp += TIM1CH1_CAPTURE_VAL;			//得到总的高电平时间4
			set = (float)temp/2000*168;
			TIM2_PWM_Init(19999,83,temp);	//初始化
		  printf("Hight: %d %f us\r\n", temp, set);
			TIM1CH1_CAPTURE_STA = 0; //开启下一次中断。
		}
		OSSemPost(ReceiverSem);
		OSTimeDly(50);
	}
}

void TASK_MPU6050(void *pdata){
	INT8U err;
	while(1){
	OSSemPend(Synchronize,1000,&err);	
	MPU_Get_Accelerometer(&aacx,&aacy,&aacz);	//得到加速度传感器数据
  MPU_Get_Gyroscope(&gyrox,&gyroy,&gyroz);	//得到陀螺仪数据
	//发送到端口
		
	OSSemPost(Synchronize);
		OSTimeDly(400);
	}
}

//转化成字符串
void ShortTo0xString(char *str, short x)
{
		unsigned short t = x;
    str[0] = '0';
    str[1] = 'x';
    for (int i = 5; i >= 2; i--)
    {
        if (t % 16 >= 10)
            str[i] = t % 16 - 10 + 'A';
        else
            str[i] = t % 16 + '0';
        t /= 16;
    }
    str[6] = 0;
}

void TASK_HM10(void *pdata){
	INT8U err;
	
	while(1){
		OSSemPend(Synchronize,1000,&err);
		USARTSendString(USART6,"\naacx:");
			ShortTo0xString(tmp, aacx);
			USARTSendString(USART6,tmp);
					 
			USARTSendString(USART6,"\naacy:");
		
			ShortTo0xString(tmp, aacy);
			USARTSendString(USART6,tmp);
					 
			USARTSendString(USART6,"\naacz:");
			ShortTo0xString(tmp, aacz);
			USARTSendString(USART6,tmp);
					 
			USARTSendString(USART6,"\ngyrox:");
			ShortTo0xString(tmp, gyrox);
			USARTSendString(USART6,tmp);
					 
			USARTSendString(USART6,"\ngyroy:");
			ShortTo0xString(tmp, gyroy);
			USARTSendString(USART6,tmp);
						
			USARTSendString(USART6,"\ngyroz:");
			ShortTo0xString(tmp, gyroz);
			USARTSendString(USART6,tmp);
			
			OSSemPost(Synchronize);
			OSTimeDly(400);
	}
}


//void TASK_RECEIVER(void *pdata){
//	OSSemPost(ReceiverSem);
//	
//	
//	OSTimeDly(200);
//}

void SystemClock_Config()
{	
	 SysTick_Config(SystemCoreClock/1000);
}
int main(){
	
	SystemClock_Config();
	OSInit();
  RCC_ClocksTypeDef get_rcc_clock;
	RCC_GetClocksFreq(&get_rcc_clock);
	Synchronize=OSSemCreate(1);
	ReceiverSem=OSSemCreate(1);
	
	OSTaskCreate(TASK_INIT, (void *)0, (void *)&TASK_INIT_STK[TASK_INIT_STK_SIZE-1], TASK_INIT_PRIO);
	OSTaskCreate(TASK_HM10, (void *)0, (void *)&TASK_HM10_STK[TASK_HM10_STK_SIZE-1], TASK_HM10_PRIO);
	OSTaskCreate(TASK_MPU6050, (void *)0, (void *)&TASK_MPU6050_STK[TASK_MPU6050_STK_SIZE-1], TASK_MPU6050_PRIO);
	OSTaskCreate(TASK_PWM, (void *)0, (void *)&TASK_PWM_STK[TASK_PWM_STK_SIZE-1], TASK_PWM_PRIO);
//	OSTaskCreate(TASK_RECEIVER, (void *)0, (void *)&TASK_RECEIVER_STK[TASK_RECEIVER_STK_SIZE-1], TASK_RECEIVER_PRIO);
//	
	OSStart();
}
