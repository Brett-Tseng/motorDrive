#include "main.h"
#include "AdvanceTim.h"

void TIM1_Cap_Init(u16 arr, u16 psc)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	TIM_ICInitTypeDef TIM1_ICInitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);		//时钟TIM1
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);		//使能GPIOA
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;			//复用
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;		//速度100MHz
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;					//复用推免输出
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;   //下拉
	
	//PA8
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_8;							
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	//PA9
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_9;							
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	//PA10
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_10;							
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	//PA11
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_11;							
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource8, GPIO_AF_TIM1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_TIM1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_TIM1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource11, GPIO_AF_TIM1);
	
	
	//初始化定时器TIM1

	TIM_TimeBaseStructure.TIM_Period = arr;							//设置计数器自动重装值 	通常为：0xFFFF
	TIM_TimeBaseStructure.TIM_Prescaler = psc;  					//预分频器，psc = 1M = 1us
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; 
	TIM_TimeBaseStructure.TIM_RepetitionCounter = 0;		//重复计数设置
	
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStructure); //参数初始化
	
	TIM_ClearITPendingBit(TIM1, TIM_FLAG_Update);		//清除TIM1更新中断标志
	
	//初始化TIM1的输入捕获参数
	//TIM1_ICInitStructure.TIM_Channel = TIM_Channel_1; 			//cc1s = 01  通道选择
	TIM1_ICInitStructure.TIM_ICPolarity =TIM_ICPolarity_Rising;			//上升沿捕获
	TIM1_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;//映射到TI1上
	TIM1_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;					//不分频，直接捕获
	TIM1_ICInitStructure.TIM_ICFilter = 0x00;			
	
  //cc1s=0x
	TIM1_ICInitStructure.TIM_Channel = TIM_Channel_1; 			
	TIM_ICInit(TIM1, &TIM1_ICInitStructure);
	TIM1_ICInitStructure.TIM_Channel = TIM_Channel_2; 			
	TIM_ICInit(TIM1, &TIM1_ICInitStructure);
	TIM1_ICInitStructure.TIM_Channel = TIM_Channel_3; 			
	TIM_ICInit(TIM1, &TIM1_ICInitStructure);
	TIM1_ICInitStructure.TIM_Channel = TIM_Channel_4; 			
	TIM_ICInit(TIM1, &TIM1_ICInitStructure);
	
	//允许更新中断，允许CC1IE捕获中断
	TIM_ITConfig(TIM1,TIM_IT_CC1|TIM_IT_Update, ENABLE);	 		
	TIM_ITConfig(TIM1,TIM_IT_CC2|TIM_IT_Update, ENABLE);	
	TIM_ITConfig(TIM1,TIM_IT_CC3|TIM_IT_Update, ENABLE);	
	TIM_ITConfig(TIM1,TIM_IT_CC4|TIM_IT_Update, ENABLE);	
	
  TIM_Cmd(TIM1, ENABLE ); 
	
  //中断分组初始化
	NVIC_InitStructure.NVIC_IRQChannel = TIM1_CC_IRQn ;			//捕获中断：TIM1_CC_IRQn; 	TIM1的触发中断：TIM1_TRG_COM_TIM11_IRQn 
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;			//抢占优先级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0; 						//从优先级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; 
	NVIC_Init(&NVIC_InitStructure);                 
	
	//TIM_ITConfig(TIM1,TIM_IT_CC1|TIM_IT_Update, ENABLE);	 		//允许更新中断，允许CC1IE捕获中断

	//TIM_Cmd(TIM1, ENABLE ); 
	
}
	
void TIM2_PWM_Init(u32 arr, u32 psc, u32 pls)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    TIM_OCInitTypeDef TIM_OCInitStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);  //使能TIM2时钟
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE); 
/*
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;          //GPIOA0
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;       //复用功能
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz; //速度100MHz
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;     //推挽复用输出
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;       //上拉
    GPIO_Init(GPIOA, &GPIO_InitStructure);             //初始化PA0
*/
	  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;       //复用功能
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz; //速度100MHz
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;     //推挽复用输出
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;       //上拉
	  
	  //GPIOA0
	  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;        
    //初始化PA0		
	  GPIO_Init(GPIOA, &GPIO_InitStructure);  
		
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;          
	  GPIO_Init(GPIOA, &GPIO_InitStructure);             
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;          
	  GPIO_Init(GPIOA, &GPIO_InitStructure);             
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;          
	  GPIO_Init(GPIOA, &GPIO_InitStructure);             
	
	
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_TIM2); //GPIOA0复用为定时器2
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource1, GPIO_AF_TIM2);
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_TIM2);
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_TIM2);

    TIM_TimeBaseStructure.TIM_Prescaler = psc;                  //定时器分频
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; //向上计数模式
    TIM_TimeBaseStructure.TIM_Period = arr;                     //自动重装载值
    TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;

    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); //初始化定时器2

    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = pls;
		
		//根据T指定的参数初始化外设TIM2OC1
    TIM_OC1Init(TIM2, &TIM_OCInitStructure); 
		TIM_OC2Init(TIM2, &TIM_OCInitStructure); 
		TIM_OC3Init(TIM2, &TIM_OCInitStructure); 
		TIM_OC4Init(TIM2, &TIM_OCInitStructure);  
		
    TIM_OC1PreloadConfig(TIM2, TIM_OCPreload_Enable);
		TIM_OC2PreloadConfig(TIM2, TIM_OCPreload_Enable);
		TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);
		TIM_OC4PreloadConfig(TIM2, TIM_OCPreload_Enable);

    TIM_Cmd(TIM2, ENABLE);
}	
