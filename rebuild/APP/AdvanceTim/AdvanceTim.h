#ifndef _ADVANCETIM_H
#define _ADVANCETIM_H
#include "main.h"

void TIM1_Cap_Init(u16 arr, u16 psc);
void TIM2_PWM_Init(u32 arr, u32 psc, u32 pls);

#endif
