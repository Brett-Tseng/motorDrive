//#include "stm32f4xx_it.h"
#include "ucos_ii.h"
#include "main.h"

u8 TIM1CH1_CAPTURE_STA = 0; //输入捕获中断
u16 TIM1CH1_CAPTURE_VAL; //输入捕获值

//蓝牙中断
//void USART6_IRQHandler(void) {
//#if OS_CRITICAL_METHOD == 3u                               /* Allocate storage for CPU status register */
//    OS_CPU_SR  cpu_sr = 0u;
//#endif
//	OS_ENTER_CRITICAL();
//	OSIntEnter();
//	OS_EXIT_CRITICAL();
		
//	OSIntExit(); 
	
//}


void TIM1_IRQHandler(void)
{
	#if OS_CRITICAL_METHOD == 3u                               /* Allocate storage for CPU status register */
    OS_CPU_SR  cpu_sr = 0u;
#endif

	
	OS_ENTER_CRITICAL();
	OSIntEnter();
	OS_EXIT_CRITICAL();
	
	if((TIM1CH1_CAPTURE_STA & 0x80)==0) //还未捕获成功
	{
		if(TIM_GetITStatus(TIM1,TIM_IT_Update) != RESET) //已经获得高电平
		{
			if(TIM1CH1_CAPTURE_STA & 0x40) //捕获到一个up
			{
				if((TIM1CH1_CAPTURE_STA & 0x3F)==0x3F) //高电平太长
				{
					TIM1CH1_CAPTURE_STA |= 0x80; //标记成功捕获一次
					TIM1CH1_CAPTURE_VAL = 0xFFFF;
				}
				else TIM1CH1_CAPTURE_STA++;
			} 
		}
		if(TIM_GetITStatus(TIM1,TIM_IT_CC1) != RESET) //捕获1发生捕获事件
		{
			if(TIM1CH1_CAPTURE_STA & 0X40)		//捕获到一个up
			{
				TIM1CH1_CAPTURE_STA |= 0x80; //标记成功捕获到一次down
				TIM1CH1_CAPTURE_VAL = TIM_GetCapture1(TIM1); 
				TIM_OC1PolarityConfig(TIM1,TIM_ICPolarity_Rising); //CC1p = 0 设置上升沿捕获
			}
			else //还未开始，第一次捕获上升沿
			{
				TIM1CH1_CAPTURE_STA = 0; 
				TIM1CH1_CAPTURE_VAL = 0; 
				TIM_SetCounter(TIM1,0);
				TIM1CH1_CAPTURE_STA |= 0x40; //标记捕获到了上升沿
				TIM_OC1PolarityConfig(TIM1,TIM_ICPolarity_Falling); //ccip = 1 设置为下降沿捕获
			}
		}
	}
	TIM_ClearITPendingBit(TIM1,TIM_IT_Update | TIM_IT_CC1); //清除中断标志
  OSIntExit();
}


/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
	/* Go to infinite loop when Hard Fault exception occurs */
	while (1)
	{
	}
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
	/* Go to infinite loop when Memory Manage exception occurs */
	while (1)
	{
	}
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
	/* Go to infinite loop when Bus Fault exception occurs */
	while (1)
	{
	}
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
	/* Go to infinite loop when Usage Fault exception occurs */
	while (1)
	{
	}
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
// void OS_CPU_PendSVHandler(void)
// {
// }

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
//void OS_CPU_SysTickHandler(void)
//{
//	if (OSRunning == 1) //当os开始运行才跑这个
//  {
//    OSIntEnter(); //进入中断
//    OSTimeTick(); //调用ucos的时钟服务器
//    OSIntExit();  // 触发任务切换软中断
//  }
//}
void OS_CPU_SysTickHandler(void)
{	
	OS_CPU_SR  cpu_sr;
	TimingDelay_Decrement();

	OS_ENTER_CRITICAL();                         /* Tell uC/OS-II that we are starting an ISR          */
	OSIntNesting++;
	
	OS_EXIT_CRITICAL();

	   OSTimeTick();                      /* Call uC/OS-II's OSTimeTick()                       */
	
	OSIntExit();                                 /* Tell uC/OS-II that we are leaving the ISR          */
}

/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f4xx.s).                                               */
/******************************************************************************/

/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
